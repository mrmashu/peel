#include "AssetCompiler.hpp"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

AssetCompiler::AssetCompiler()
{
	compilecount = 0;
	assetcount = 0;
}
void AssetCompiler::compile()
{
	validate();
	std::cout << "building assets.." << std::endl;
	compile_dir("./assets");
	std::cout << "compiled " << compilecount << " out of " << assetcount << " assets" << std::endl;
}
void AssetCompiler::validate()
{
	// make sure there's an assets folder:
	DIR* dir = opendir("./assets");
	if(dir == NULL){
		throw std::runtime_error("no assets directory");
	}
	closedir(dir);
	
	// in this context, there is an assets folder, so create a corresponding resources folder:
	dir = opendir("./resources");
	if(dir == NULL){
		// create the resources directory because it doesn't exist:
		mkdir("./resources", 0777);
	}
}
bool AssetCompiler::should_skip(std::string assetpath)
{
	// figure out the resource path based on asset path:
	std::string assets_literal = "assets";
	int assets = assetpath.find(assets_literal);
	std::string resourcepath = assetpath;
	resourcepath.replace(assets, assets_literal.size(), "resources");
	
	// first of all, if the resource doesn't exist, don't skip it:
	if(access(resourcepath.c_str(), F_OK) != 0){
		return false; // don't skip, the resource doesn't exist
	}
	if(access(resourcepath.c_str(), W_OK) != 0){
		throw std::runtime_error(std::string("failed to compile asset ") + assetpath + " because the resource file is not writable");
	}
	
	// in this context, a resource exists and is writable, and the asset is readable.
	// check the timestamps:
	time_t asset_time, resource_time;
	struct stat statbuf;
	if(stat(assetpath.c_str(), &statbuf) == -1){
		throw std::runtime_error(std::string("failed to compile asset ") + assetpath + ". failed to stat asset file.");
	}
	asset_time = statbuf.st_mtime;
	if(stat(resourcepath.c_str(), &statbuf) == -1){
		throw std::runtime_error(std::string("failed to compile asset ") + assetpath + ". failed to stat resource file.");
	}
	resource_time = statbuf.st_mtime;

	// if the asset file is newer than the resource file, don't skip it:
	if(asset_time >= resource_time) return false;

	// otherwise, we can safely skip it!
	return true;
}
void AssetCompiler::compile_file(std::string path)
{
	std::cout << "compiling file " << path << std::endl;
	// don't do anything if it should skip it:
	assetcount++;
	if(should_skip(path)) return;
	compilecount++;
	
	// detect file extension and delegate based on that
	std::size_t dot = path.find_last_of('.');
	std::string ext = path.substr(dot+1);
	if(ext == "blend"){
		cblend(path);
	}else{
		compilecount--; // we didn't actually compile this one
		std::cerr << "asset extension " << ext << " not supported yet. tell mistermashu he's lazy." << std::endl;
	}
}
void AssetCompiler::compile_dir(std::string path)
{
	std::cout << "compiling dir " << path << std::endl;
	DIR* dir = opendir(path.c_str());
	if(dir == NULL){
		throw std::runtime_error(std::string("error in AssetCompiler::compile_dir(): dir ") + path + " doesn't exist.");
	}
	struct dirent* dp;
	while((dp = readdir(dir)) != NULL){
		if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
		std::cout << "in while " << dp->d_name << std::endl;
		// if it's a dir, compile dir. otherwise, compile file
		std::string fullpath = path + '/' + dp->d_name;
		if(dp->d_type == DT_DIR){
			compile_dir(fullpath);
		}else if(dp->d_type == DT_REG){
			compile_file(fullpath);
		}else{
			std::cerr << "unable to compile asset file at " << path << " because it is not a directory or a file." << std::endl;
		}
	}
	closedir(dir);
}
void AssetCompiler::cblend(std::string path)
{
	// TODO compile a blend file into a nice .banm
	std::cout << "TODO compile blender file " << path << std::endl;
}
