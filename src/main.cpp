#include "AssetCompiler.hpp"
#include <iostream>

void usage()
{
	std::cout << "USAGE:" << std::endl;
	std::cout << "peel clean: removes the resources directory" << std::endl;
	std::cout << "peel assets: compiles everything from /assets and puts them in /resources" << std::endl;
	std::cout << "peel build: builds game source and build assets into resources" << std::endl;
}

void assets()
{
	AssetCompiler asset_compiler;
	asset_compiler.compile();
}

void clean()
{
	// TODO delete the resources folder
}

void build()
{
	// TODO build assets and code
}

void code()
{
	// TODO build just the code
}

int main(int argc, char** argv)
{
	if(argc <= 1){
		usage();
	}else{
		if(strcmp(argv[1], "assets") == 0){
			assets();
		}else if(strcmp(argv[1], "clean") == 0){
			clean();
		}else if(strcmp(argv[1], "build") == 0){
			build();
		}else if(strcmp(argv[1], "code") == 0){
			code();
		}else{
			usage();
		}
	}
	
	return 0;
}