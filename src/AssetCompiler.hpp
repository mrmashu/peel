#pragma once
#include <string>

class AssetCompiler
{
	private:
		int assetcount;
		int compilecount;
		void validate();
		bool should_skip(std::string filepath);
		void compile_file(std::string path);
		void compile_dir(std::string path);
		void cblend(std::string path);
	public:
		AssetCompiler();
		void compile();
};