# General
Peel is a middleware tool that is to be used while making games on the banana engine.

# Usage
- `peel` will check what needs to be built and build it
- `peel run` will build and run the game
- `peel debug` will build with debug flags and run
- `peel test` will build with TEST flag and run tests

# Assets get Build to Resources
- `.blend` -> `.bm`
- `.png` -> `.bt`

# Types of Resources
- `.bm` is a Banana Model file
- `.bt` is a Banana Texture file

# Contributors
- MisterMashu (mistermashu.com)